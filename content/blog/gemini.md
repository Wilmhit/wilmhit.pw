---
title: I invested in Gemini
dateReleased: 2023-12-16
---

From the very begginng of this website I planned to do a Gemini version. You
can see that by the way the formatting of links is done on all my blog posts.

The time is finally here! I made my own blog on Gemini protocol like all the
cool kids. Let me tell you a bit more about that experience and maybe I'll
convince you to join us.

### Tech stack

I approached Gemini knowing only about two browsers: Amfora and Lagrange.
Former is terminal client while latter is graphical one. Later I also came
across an Android client Buran. I can easily recommend all three of them.

[Lagrange](https://gmi.skyjake.fi/lagrange/)

[Amfora](https://github.com/makew0rld/amfora)

[Buran](https://f-droid.org/packages/corewala.gemini.buran/)

I didn't know any good server. At first I tried to look up some of the
solutions online. Thankfully someone created an awesome Gemini list which
allowed me quickly review what I need.

[Awesome Gemini list](https://github.com/kr1sp1n/awesome-gemini#graphical)

After some experimentation I settled on `gmid`. My reasons are as
follows:
 - It supports SNI multiplexing. This is important for me as I knew already
   the same server will be used to host multiple capsules.
 - Compilation went without problems.
 - Really easy and straght-forward configuration. Brings to mind nginx but
   without all those pesky HTTP stuff.
 - Is written in C so no extra runtimes will be needed on target system.
 - Supports autoindexing.
 - Looks like actively maintained.

It also supports fastCGI and reverse proxy support which I don't use but is
nice to see. Later I came also to appreciate the `gg` binary that comes bundled
with the package. It is something like `curl` but for Gemini.

[gmid website](https://gmid.omarpolo.com/)

One additional note is that on Gemini having
self-signed certificate for website is normal. I really like that. Issuing
certificates at trusted third-party is an big undertaking but on normal (www)
browser you cannot even have encryption without that (self-signed certificates
are regarded by browsers as not safe). Gemini takes here different approach. By
making it easy to issue a certificate, we can regard all sites without it as
unsafe. As result - your connection to virtually every capsule is TLS-secured.
Very often with newest TLS versions.

### Rewriting Hugo to Python

Now comes the big part. What do I do with my website written to support Hugo
to start serving it via Gemini?

I don't have an easy anwser to that. The first try was to just copy the index
page and that was it for 2 weeks. All the links (all 2 of them) redirected to
HTTP website. There was no blog. It was a happy start.

You can already read this post via Gemini because I already started making my
own SSG. What I'm doing right now is just copying blog posts from `content`
directory to `blog` directory for gemini website.

#### Still very WIP

I'll be slowly making improvements to this setup. As of now we still lack:
 - Proper formatting. Gemini markdown (if that's what it is called) differs
   significanyly from normal one. The text is readable so that's not an urgent
   problem.
 - I'll need to include images. I have a blog post WIP that will include a
   measurable number of those. These will also need to be properly scaled.
 - Index page for blog. Current setup is based on directory listing (`index of
   blog/`).

For now I plan to use Python to supplement Hugo generation. A tool that is able
to generate both website and capsule is a nice thought. I may spend some time
on it but currently I have my hands full.
