---
title: New project: ffmpreg
dateReleased: 2022-06-24
---

Recently I had a rather insightful conversation. One of the Fedizens had a
problem with the ffmpeg command line utility. What was it?

<!--more--> 

Ffmpeg uses a lot of switches and options. It is very hard to remember all of
it. Even if you do remember, you need to type all this:

```
ffmpeg -i input_video.avi -codec:audio aac -b:audio 128k -codec:video libx264-crf 23 output_video.mp4
```

Command above will convert AVI video file to MP4 encoded with AAC@128kb and
h264@CRF23

```
ffmpeg -i input_video.mp4 -codec:video libvpx-vp9 -crf 30 -b:video 0 -codec:audio libopus -vbr on -threads number_of_threads output_video.webm
```

And this one converts MP4 to WEBM encoded with opus audio and VP9 video.

Long, aren't they? I really am no expert in audio and video codecs so I could
really use a little bit of a hint. History would also be useful.

Since there is no way I could remember I've found those with `curl
cheat.sh/ffmpeg`.

Here is what [mikapyon](https://mastodon.social/@mikapyon) from mastodon.social
had to say (or write):

> I frequently do like: {{< break >}}
> `ffmpeg -ss 100 -i pururun.mkv -map_metadata -1 -sn -ar 44100 -ab 128k -s 1280x720 -r 30 pururun.mp4` {{< break >}}
> I type all of that out because half I want to remember and half because NO CLUE how to make aliases for that.
> The problem with aliases is I want to change or omit certain flags from time to time (´-﹏-\`；)

[Link to original post](https://mastodon.social/@mikapyon/107121537422411691).

## Solution requirements

Valid solution should fulfil these points:
 - Be short to type
 - Easily changeable input and output filenames
 - Other options should be also easy to change
 - Offer description to all the options
 - Have history of typed commands

Many of these points can be accomplished by Your shell. However typing `--help`
every time you want to check what particular option does just to get lost
in this ridiculously long help page (or man) is not very practical.

## My solution

My train of thoughts began with a simple CLI utility able to set profiles and
save them. Like so:

```
ffmpreg -p 1
```

would execute ffpeg with whatever was saved in profile 1 (along with the
filenames).

```
ffmpreg -p 1 -i my_file.avi -crf 23
```

to change a few options.

```
ffmpreg -p save1 -i my_file.avi -crf 23 output.mp4
```

to save those options to profile 1.

```
ffmpreg -p 1 -p save1 -crf 23
```

to overwrite CRF option from profile 1 and save it.

Those profiles could of course be 2, 3 or anything you like. I think of
limiting number of profiles to e.g. 20 to limit the size of data file.

### Where is the promised history, you ask?

In order to provide usage history along with option description ffmpreg needs
TUI mode. I'm thinking of something like this:

![The ffmpreg TUI draft](/images/ffmpreg-draft.png)

The options divided into tabs speed up navigation and also help You find what
you need even if You don't know what is it called. Active tab is marked with a
dot.

Navigation on tabs is done via \<left>/\<right> or h/l. Option list can be
scrolled with j/k or \<up>/\<down>.

History tab is special. It will allow you to view all previously executed
commands even those issued from CLI. Upon hitting enter on particular item You
will be presented with possibilities to either execute it right now or load
this into TUI.

On the bottom of the screen is a quick guide to other keys: 
 - "q" to quit.
 - F1 - F10 to save to first 10 profiles.
 - F11 to save to other higher profile (popup window).
 - Undecided key to run. "r" might be what works the best.

Options themselves will consist of:
 - Name
 - Value
 - Short description (probably taken from `--help` or man page for ffmpeg).
 - Possible values

### Summing up

TUI interface will make the program useful for newcommers, while smart command
line interface will keep those more advanced happy. I think that combo will
satisfy us all. 

I already started working on this. Repository is located on my GitLab. I use
Rust programming language and pancurses TUI library.

I will continue to work on this in my free time. There is also a bit of
organizational work that needs to be done. ffmpreg needs a website and a solid
logo.

Since I'm really bad at doing anything art-related logo might be the biggest
problem. I honestly don't have any idea what a good logo should include.
