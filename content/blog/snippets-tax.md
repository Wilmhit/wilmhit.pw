---
title: Big Tech nie kradnie treści prasie
dateReleased: 2022-04-28
---

This article is a translation of Cory Doctorow's Medium article available at
the link below. Footnotes were added in translation.

<!--more--> 

Ten artykuł jest tłumaczeniem artykułu autora Cory Doctorow opublikowanego na
portalu Medium. Oryginalny artykuł jest dostępny do przeczytania pod poniższym
linkiem. Przypisy zostały dodane podczas tłumaczenia.

[Big Tech Isn’t Stealing News Publishers’
Content](https://pluralistic.net/2022/04/18/news-isnt-secret/#bid-shading)

---

Kraje dookoła świata - Francja, Australia, Brazylia, a teraz nawet Kanada
(projekt ustawy C-11) -  ostatnio zakochały się w pomyśle zasugerowanym przez
media. Dotyczy on wprowadzenia praw licencyjnych do kopiowania urywków
artykułów, których linki użytkownicy publikują na portalach
społecznościowych.

Prawo licencyjne każdego kraju ma zestaw ograniczeń i wyjątków (tak jak prawo
cytatu i określone dozwolone użytki), które zapewniają zgodność tego prawa z
prawem wolności słowa. Historycznie, cytowanie prasy wpisywało
się w takie wyjątki. Coś będzie "niusem", jeśli można o tym mówić. W przeciwnym
wypadku będzie to tajemnica.

Od dekad nasze fora przenoszą się do sieci. Pandemia jedynie przyśpieszyła ten
proces. Dzisiaj jeśli rozmawiamy o jakimś artykule, to najpewniej robimy to na
jakiejś platformie internetowej, a poprzez monopolizację liczba takich platform
jest coraz mniejsza, a same platformy nadzwyczajnie przychodowe.

Rządy państw (poprawnie) zaobserwowały, że do demokracji potrzebne jest wolna
media oraz (poprawnie) zaobserwowały, że są one w tarapatach. Ponadto,
(poprawnie) został wyciągnięty wniosek, że monopole w sektorze technologicznym
mają coś wspólnego z tą sytuacją.

Pomimo to, również został wyciągnięty (niepokojąco niepoprawny) wniosek, że
rozwiązaniem problemu jest wprowadzenie nowych praw pseudo-autorskich, które
pozwolą publikatorom wyciągać zyski z prawa do rozmawiania o prasie.

Co jest nie tak z tym wnioskiem? Zacznijmy od tego, że jest on porażką patrząc
z perspektywy praw człowieka. Jeśli masz prawo licencjonować dyskusje o pewnych
artykułach, to masz również prawo wstrzymać to licencjonowanie. Oznacza to, że
będziesz mógł odsunąć niektórych od debatowania lub analizowania twojego
dziennikarstwa.

Ponadto, cały pomysł błędnie podchodzi do tego jak giganci technologiczni
oszukują prasę. Cytowanie artykułów nie jest naruszeniem praw autorskich, zatem
nieprawdą będzie powiedzenie, że platformy internetowe kradną ich treść. Prawda
jest taka, że platformy zabierają pieniądze wydawcom.

Mam tu na myśli oszustwa reklamowe. Przemysł reklam internetowych jest
zdominowany przez duopol Googla i Facebooka. Stało się tak jawnie wbrew prawu i
do dzisiaj jest to przedsięwzięcie na wskroś nielegalne:

[Antykonkurencyjne przejęcia](
https://www.ftc.gov/news-events/news/press-releases/2020/12/ftc-sues-facebook-illegal-monopolization
)

[Wygaszanie respektujących prywatność rywali](
https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3247362
)

[Uniwersalne śledzienie bez zgody ludzi, nawet tych, którzy nigdy nie korzystali z usług](
https://ico.org.uk/media/about-the-ico/documents/4019050/opinion-on-data-protection-and-privacy-expectations-for-online-advertising-proposals.pdf
)

[Tajne umowy manipulujące rynkiem reklamowym](
https://techcrunch.com/2022/03/11/google-meta-jedi-blue-eu-uk-antitrust-probes/
)

[Regularnie rejestrowane przekręty na rynku reklam internetowych (kto płaci,
ile, komu, za co, komu pokazywane są reklamy, ile razy)](
https://www.niemanlab.org/2021/09/well-this-puts-a-nail-in-the-news-video-on-facebook-coffin/
)

Blisko połowa pieniędzy przeznaczana na reklamy ląduje w portfelach tych dwóch
gigantów. Jest to największy przekręt w internecie. Wart więcej niż zsumowane
przekręty wszystkich grup hakerskich. Jeśli wydawcy mogliby korzystać z
nieustawionego rynku reklamowego, to ich zyski podniosłyby się znacząco. To
są pieniądze, które im się należą.

Jak sprawić, żeby rynek reklamowy nie był ustawiony? Są pewne kroki, które
podjęte tu i teraz, dadzą natychmiastowe efekty. Szybciej niż rozbijanie gigantów
na mniejsze firmy (aczkolwiek to też jest dobry pomysł).

 - Zablokowanie transakcji, gdzie pojedyncza firma jest pośrednikiem (agentem)
 zarówno dla strony kupującej, jak i sprzedającej.

 - Wprowadzić kary w stylu ustawy Sarbanesa-Oxleya [^1] za nadużycia
 finansowe.

 - Narzucić obowiązkowe (audytowane) sprawozdania, w które zawierałyby
 informacje o reklamach - ile danych zostało zebranych, ile przekazanych,
 gdzie, na jakie cele.

 - Zbudować system odszkodowań finansowych za przekręty reklamowe w tym
 ustawione przetargi.

 - Zacząć wymagać aby przetargi reklamowe były licytacjami otwartymi typu _header_[^2].

 - Zabronić reklam opartych o śledzenie użytkowników, co wyeliminuję przewagę gigantów
 posiadających najwięcej danych. Zarazem rozwinie to rynki reklam kontekstowych
 (opierających się o to co jest czytane, a nie o to kto czyta).

 - Nakazać jawność kryteriów na których bazuje pozycja artykułów w
 rezultatach wyszukiwań i na portalach społecznościowych.

Wszystkie te rzeczy pomogą wydawcom bez potrzeby tworzenia niebezpiecznych
licencji na dyskusje o wiadomościach. Zatem czemu duże firmy medialne wolałyby
jednak rozwiązanie licencyjne:

Po krótce mówiąc: Giganci prasowi są w każdym stopniu tak zepsuci jak
giganci technologiczni. Media są chore na maniakalny kult pieniądza znacznie
dłużej niż firmy sektora technologicznego. Na długo przed komercjalizacją
internetu orgia korporacyjnych przejęć uderzyła w gazety łącząc je,
zabijając lokalne dziennikarstwo, konsolidując sprzedaż, zwolniając
niewygodnych dziennikarzy. Nowi korporacyjni właściciele przejmowali również
fundusze rezerwowe gazet, sprzedając zasoby i upodatniając gazety na szoki
finansowe.

Tak właśnie media, które przeżyły epokę telegrafu, radia, kablówek i
telewizji satelitarnej weszły w epokę internetu. Bez zasobów finansowych z którymi można
byłoby pozwolić sobie na eksperymenty, zależni od właścicieli lokalów do
których zostały sprzedane budynki.

Gwarantowana porażka mediów przy przejściu do epoki cyfrowej sprawiła, że firmy
były jeszcze słabsze, otwierając drogę do jeszcze bardziej intensywnego
gwałtu finansowego na prasie. Zastrzyki prywatnego kapitału zmieniły kiedyś
osławione gazety w dzisiejsze gloryfikowane ulotki w sklepach spożywczych z
ekipą dziennikarską nie pracującą w terenie, lecz w piwnicy o rozmiarach
podobnych do przeciętnej pizzerni.

[Gazeta Chicago Tribune jest zabijana na naszych oczach](
https://pluralistic.net/2021/10/16/sociopathic-monsters/#all-the-news-thats-fit-to-print
)[^3]

Bilionerzy żerujący na mediach to nie arystokraci z bogatych rodzin i
poczuciem obywatelskiego obowiązku. Są to wampiry z odległych krajów, którzy
przychodzą by podnieść ceny, obniżyć jakość, zmiażdżyć związki zawodowe oraz
obniżyć wypłaty pracowników.

[Tajemniczy fundusz inwestycyjny zabija gazety](
https://www.theatlantic.com/magazine/archive/2021/11/alden-global-capital-killing-americas-newspapers/620171/
)

A skrajnie prawicowi ideolodzy pilnują kociołków sępich kapitalistów, których
bezwstyd nie zna granic. Są w stanie poświęcić całe wydania na wychwalanie cnót
całkowicie nieregulowanego kapitalizmu.

[Manifesto kapitalizmu: W energetyce potrzeba bardziej wolnego rynku, a nie
więcej socjalizmu](
https://nationalpost.com/opinion/the-capitalist-manifesto-in-energy-we-need-more-free-markets-not-more-socialism
)

Nawet wtedy, gdy żądają zapomóg od rządu.

[Andrew Coyone: Kiedy czytasz jak rząd ratuje media, przechodzą cię ciarki](
https://nationalpost.com/opinion/andrew-coyne-its-when-you-read-details-of-media-bailout-that-the-chill-sets-in
)

Stylują się na zbawców prasy, po czym kupują krajową gazetę, aby zrobić z niej
ulotkę reklamową swojego kasyna online.

["The Star" prowadzi hazard etyką](
https://www.canadaland.com/the-star-gambles-with-ethics/
)

Dzisiejsi baronowie medialni nie są obrońcami demokracji. Są dla niej
zagrożeniem.

[Biden nazywa Murdoch "najbardziej niebezpiecznym człowiekiem świata" - nowa
książka podaje](
https://www.cnn.com/2022/04/03/media/reliable-sources-biden-murdoch-fox-news/index.html
)[^4]

Ta gołota nadająca się tylko pod gilotynę *uwielbia* opodatkowywać wycinki
artykułów. Weźmy np. Ruperta Murdoch, który wykorzystał taki właśnie podatek w
Australii, aby zawrzeć bardzo korzystną umowę z Googlem i Facebookiem. Umowa
zostawia małe lokalne gazety na lodzie. Właśnie te gazety, które wypełniały
pustkę po tym jak Murdoch wykupił i zabił lokalne redakcje.

[Facebook zapłaci News Corp za treści w Australii](
https://www.bbc.com/news/world-australia-56410335
)

We Francji największe koncerny medialne przygotowały system podatków od
wycinków artykułów, który zmusza gazety do używania Google Showcase
jednocześnie zwiększając zależność całej branży od Google.

[Transakcja pomiędzy Google a francuską prasą musi poczekać na decyzję w
dochodzeniu antymonopolowym](
https://www.reuters.com/technology/exclusive-google-deal-with-french-publishers-hold-pending-antitrust-decision-2021-06-29/
)

Więc dlaczego media nie chcą odblokować rynku reklamowego? Dlaczego tak bardzo
koncentrują się na opłatach za wycinki artykułów? Dlatego, że wiedzą że uczciwy
rynek reklamowy będzie sprzyjał każdemu - w tym redakcjom niezależnym. Podatek od
linków jest dobrą podstawą, aby układać wygodne umowy pomiędzy mediami a
technologicznymi monopolistami. 

Big Tech nie jest odpowiedzią na kryzys prasy. Podatek od linków nie spowoduje 
demokratyzacji mediów. Pomoże on jedynie gigantom medialnym i technologicznym
umocnić ich pozycje, trzymając małe firmy na dystans.

Każdy podatek od linków, który będzie na tyle duży, aby odbić się na
rynku prasy, będzie za duży, aby mała firma technologiczna lub parę małych
firm lub organizacja non-profit mogła go zapłacić.

Może się wydawać że przekształcenie rynku reklamowego jest zadaniem
niemożliwym, ale nigdy jeszcze nie było czasu tak dobrego jak teraz.
Publika nie patrzy przyjaźnie na komercyjną inwigilację. Również reklamodawcy
zmądrzeli już na tyle, żeby zauważyć jak bardzo są kantowani.

Trzeba jeszcze przekonać pracowników. Podatek od linków nie odniósł by
sukcesu w Europie czy Australii, gdyby nie niezależni dziennikarze, ten
podatek promujący. Dali się nabrać: uczynienie gigantów medialnych jeszcze
bogatszymi nie przełoży się na zwiększone wypłaty czy na nowe dochody dla
niewykupionych redakcji.

Musimy wyjść już z patologii, która nęka branże kulturowe, w której
pracownicy pogodzili się, że najlepsze na co ich stać to kibicować własnym
monopolistom i dzięki temu dostać parę okruszków jak ci się obżerają kolejnymi
wygranymi.

My możemy - my musimy - wierzyć w więcej niż tylko niewiele zmieniony podział
ciasta pomiędzy monopolistami technologicznymi i medialnymi.

[^1]: [Ustawa
  Sarbansa-Oxleya](https://pl.wikipedia.org/wiki/Ustawa_Sarbanesa-Oxleya) to
  ustawa z 2002 roku, która zwiększa dokładność raportów finansowych firm. Za
  oszystwa zostały zwiększone kary oraz odpowiedzialność została przeniesiona
  na osoby podpisujące raport. Zostały również narzucone niezależne audyty
  finansowe.

[^2]: Przetargi reklamowe dzielą się na waterfall i header. Header jest metodą
  sprawiedliwszą jako, że wszystkie oferty muszą być złożone na raz.

[^3]: Jest to artykuł Pluralistic. Właśnie czytasz pracę tego samego autora.

[^4]: Rupert Murdoch to amerykańsko-australijski biznesmen i właściciel
  korporacji medialnej "News Corp". Były właściciel portalu MySpace.
