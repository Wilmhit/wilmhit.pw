---
title: Sites I recommend
dateReleased: 2022-04-19
---

This is a list of sites I recommend you should check out!
It will be regularly updated. Check out post date for last update.

<!--more--> 

[Xe's blog](https://christine.website/blog)

[Larsenist's site](https://jrlarsen.net/)

[Join mastodon](https://joinmastodon.org/)

[Shitty services](https://shittyurl.org/)

[Linux.pizza blog](https://blogs.linux.pizza/)

[RMS](https://stallman.org)

[Motherfucking website](https://motherfuckingwebsite.com/)
[Better motherfucking website](https://bettermotherfuckingwebsite.com/)
[Best motherfucking website](https://thebestmotherfucking.website/)

[Make frontend shit again](https://makefrontendshitagain.party/)

[Igor Cudnik's site (my dear friend)](http://igor.cudnik.student.put.poznan.pl/)

[The guy who supports Linux, Unix, MacOS X, iPhone, iPad, Android, Palm WebOS, PalmOS Classic, Monochron, Pebble and TRS-80 CoCo but not Windows](https://www.jwz.org/doc/)

[Alex Russell's blog](https://infrequently.org/)

[About anti-user design](https://www.deceptive.design/)

[Phoronix news](https://www.phoronix.com/)

[Privacytech website](https://privacytech.neocities.org/)

[Comparison of email providers](https://digdeeper.neocities.org/ghost/email.html)

[Autistici collective](https://www.autistici.org/)
