---
title: Language Server Protocol
dateReleased: 2022-04-25
---

From time to time I like to use Kate. Kate is a text editor
for KDE Plasma Desktop Environment and it's a rather
programmer-centric text editor. My main use for this piece
of software is editing `.txt` files. I never bothered to
open a python script with it or any other code. There are
better editors for writing code, right?

<!--more--> 

Recently I had to write a LaTeX document. Perfect use for
Kate. My cursor hovers over *.tex* file. Double-click. Kate
opens. At the bottom of the screen I see an error. It says
that Kate could not run tslab, the language server for
LaTeX documents.

I was very surprised that a text editor that is a old and
already established text editor could get a LSP support.
After quick search turns out that Christoph Cullmann was
working on it since 2019[^1].

## My history with LSP

I first heard of LSP when I was using VSCode. I disregarded
it as Microsoft-only thing. Still, it was cool that there
was a text editor and there were also servers installed as
plugins. It meant that someone else could implement
language support even if Microsoft did not want to spend
resources on it.

In fact this happened with Rust. What I saw was a plugin
for VSCode that was not authored by Microsoft but still
implemented the Language Server Protocol.

When I finally got around to configure Neovim properly (for
a long time I was using Vim plugin in VSCode), I noticed
that community is hyped about build-in LSP support coming
to the editor. What I really wanted to try out is to
configure Neovim so it could use the Pylance server.

From my experience it is one of the only truly working LSP
servers for Python. It is really hard to make a good
language server for Python due to nature of the language. I
didn't succeed in making this installing it, but I was lucky
PyRight. I didn't know about PyRight until I read Pylance's
documentation.

The difference between PyRight and Pylance is that one is
FOSS and the other one is closed source. Pylance is based
on PyRight with additional AI-powered features (and also
telemetry). At least in theory it should provide
completions that are better than PyRight's but difference
wasn't noticeable.

At first it was hard to make any LSP server work in Neovim.
Some time later *lsp-config* plugin came along. Little bit
later Neovim version 0.5 released, stabilising the feature.
Suddenly things started working as expected. Awesome!

## The boom of LSP

Right now from time to time I hear that some editor got LSP
support, some plugin exists to make LSP work with something.
People list their favourite servers and configurations. LSP
got really popular in short amount of time[^2]. And that is
a good thing.

What at first was a VSCode extension standard is quickly
becoming the standard for implementing completions in
various editors and IDEs. Soon it won't be viable for new
IDEs to build own language support. Betting that
in-house-build completions are better then those already
established is a risky bet.

The LSP has an effect of liberating entire field of
language servers. Most of the servers implementing LSP are
FOSS and are really good. If we compare that to how things
looked 5 years back, the very best completions were
available only in closed source IDEs build for mainly
single language.

Now it is possible to get the very best code assistance in
pretty much every major editor available. Single lanugage
IDEs are slowly becoming obsolete (at least for
general-purpose programming languages).

## What is still needed

As I see it LSP is only the first step in making editor
space more open. It would be good if more features could be
shared between programs.

#### Project files

Right now there is no single major standard for managing
what is a project. One can name:

 - Visual studio has a `.vcxproj` files and also `.sln` for
*solutions*.
 - In VSCode you can *open workspace* and *open folder*.
Workspaces are stored as `.code-workspace`.
 - Vim extensions use location of `.git` folder or other
custom way.
 - Vim itself can use comments at the top of the file to
configure settings.
 - JetBrains IDEs have `.idea` folder.
 - Eclipse has `.project` file (at least file extension is
nice).
 - NetBeans has `nbproject` folder.

Also quite a lot of tooling have own standards (like
*gradle* or *npm*). It is all a mess. I could agree that all
those standards are different and information stored in
those files is unique. Nevertheless, there is a common
core for all of these. This being:

 - Where source files are located (to differentiate
between library source files).
 - What environment variables are to be set inside editor's
terminal upon startup.
 - Where is the project root.
 - Information about tabulation being used (spaces or tabs).

Storing this kind of information could be easily
standardized. What I propose is a file that _could_ be
called i.e. `project.json` in root directory of a project
that has all the information needed for basic configuration
of any editor. This file would be stored in a version
control just like any other file.

It's not necessary for an editor to read all options
included in configuration. And it is not necessary for a
user to use all the parameters from that file. If an editor
is really simple maybe it would only read how many spaces
it should put on new lines. If a user wants to locally
overwrite a value there could be a `project.local.json`
file. It should be excluded from version control. Relation
between local file and the main one would be analogical to
how Unix system treat configurations from `/etc/` and
`$HOME/.config/` directories.

Nothing stops editors from having it's own configuration
stored somewhere else (as long as it is actively
synchronized with `project.json`). I.e. JetBrains could
still check in `.idea` folders.

The exact specification of the standard would require
in-depth analysis of what editors commonly store in those
files. `.json` format could also be discussed.

#### Executing configurations

This is really simple matter. The problem is very similar
to previous one so I won't describe it throughly. What I
can say is that VSCode's format looks like it does the job
really well.

For those unfamiliar with `launch.json` I'll spare a few
words. It is a file that describes how a project should be
launched. Multiple configurations are supported and
debuggers too[^3]. Task can be set up to run before and
after launch. I. e for a project written in python this
file tells the editor that main way to run the code is via
`python3.10 src/main.py` command while virtual environment
is active.

## The future of LSP and editors

I believe LSP is a technology that will impact the
landscape of text editors in very good way. Here is some
insight in what I think might happen.

#### The end of IDEs

There is really no point of having a language-specific IDE
if a notepad program can have most of the features.

You might say that it is obviously not true, since IDEs
provide not only code assistance. To this I say: Let's look
at what JetBrains is doing. JetBrains' IDEs are probably
the most popular when summed up. We have WebStorm for
JS/TS, CLion for C/C++, PyCharm for Python and more. All of
those are really InteliJ but with another language server
instead of the Java one.

From a company perspective it would be way cheaper to
outsource the creation of language servers. And as we can
see independently-made servers are just as good if not
better than those build-in. Why not just provide support
for an LSP-compliant language server and call it a day?

I don't believe IDEs will fade out completely. Following
JetBrains example: All of those programs will drift closer
and closer together. At some point it won't matter if you
install InteliJ or PyCharm. It will be the same program
only preconfigured differently.

#### Abuse of the protocol

Designing open standards is very difficult. It is easy to
go overboard with standard's capability. We have seen this
many times. Primary example is the Web, where current state
of technology is result of standards being abused to
accommodate abuse of standards higher up the protocol stack.

Will it happen to LSP? I don't know. How LSP could be
abused? I don't know. Right now I could imagine some kind
of spellchecking done via LSP, which definitely should be
done via some other open standard and not LSP. But this
isn't as malicious use and I don't see it spouting other
miss use. But people are creative, you know?

## To sum up

I am very optimistic about what happens next. The future
looks bright for text editors. Goodbye, IDEs. You won't be
missed.

[^1]:https://cullmann.io/posts/kate-language-server-protocol-client/

[^2]:https://microsoft.github.io/language-server-protocol/implementors/tools/

[^3]:https://code.visualstudio.com/Docs/editor/debugging
