---
title: Mozilla needs to change the approach
dateReleased: 2022-11-25
---

From time to time we hear how Firefox loses popularity. At the time of writing
Firefox' market share is at 3%. This is showing. Many long-time users have
switched and developers are (very) slowly dropping support. What I want to do is
discuss how current approach have led to this.

### Mobile is not looking good

I'm going to address mobile Firefox first. It is usable. Now. After many, many
years of negligence. iOS version is based on webkit. That is enforced by the
system. There is nothing Mozilla can do about it and because of that their
browser will remain rebranded iOS forever. That takes away one of key
advantages of Firefox: being different.

On the Android front we also see deficiencies. First versions of Firefox had
severe performance issues. This is was eventually corrected but the bad taste
remained.

With many features Mozilla was too late. As an example I could list GeckoView
or Add-ons support[^1].

### Monetize-only ecosystem

The ecosystem around Firefox browser is small. There are mobile versions and
few services you can buy from Mozilla but that's it. Many of the tools that
were build for profit are still very helpful in free version (like Firefox
Relay) but pretty much all of them focus on profits. I know, money is
important. There is one big problem with this, hear me out!

Those for-profit tools will never create coherent ecosystem of apps. And I'm not
talking strictly bout user-facing.

#### Ecosystem behind the scenes

Chromium browsers have excellent tools for developers including linters, GUI
test frameworks and runtimes like node or electron. 

When a developer uses Chromium for everything, everything is guaranteed to work
on Chromium. Firefox doesn't do this. You can't run Cypress[^2] on Gecko. Or
run your app in a SpiderMonkey[^3] runtime. If someone won't put effort to test
it on Firefox it will be hit or miss.

#### What the user sees

If we talk about end user and compare Firefox to Chrome: Chrome integrates
seamlessly with all of Google's webapps. If you log into the browser you are
logged in everywhere. All of privacy cowboys will now scream "but at what
price?!". You're right. It's not worth it. Please now tell that to the average
Chrome consumer who sees absolutely no problems with it and will oppose any
change. In their eyes that is a feature. One that makes you really like Chrome.

This example we can extend to Microsoft Edge. This is the browser that comes
preinstalled and already synced! How easy is that?

If Firefox has to compete with browsers that don't play fair, it needs strong
ecosystem. A suite of apps that work flawlessly on every device you have.
Synchronized. I hope recent revival of thunderbird along with it's new Android
client is a start of that.

[Revealed: Our Plans For Thunderbird On Android](https://blog.thunderbird.net/2022/06/revealed-thunderbird-on-android-plans-k9/)

### Lack of trust

There were many opinions in this topic already. Here is mine.

A company that is sinking really doesn't need to pay investors at that moment.
Nor should increase pays for top executives. It might be hard to do, but a
company in this situation has to look like a group of angels in temporary
trouble. Writing controversial articles that will divide community in half
isn't good idea either. Massive layoffs create panic.

[Firefox usage is down 85% despite Mozilla's top exec pay going up 400%](https://calpaterson.com/mozilla.html)

[Mozilla CEO Calls for Increased Censorship: ‘We Need More Than Deplatforming’](https://www.movieguide.org/news-articles/mozilla-ceo-calls-for-increased-censorship-we-need-more-than-deplatforming.html)

[Mozilla is laying off 250 people and planning a ‘new focus’ on making money](https://www.theverge.com/2020/8/11/21363424/mozilla-layoffs-quarter-staff-250-people-new-revenue-focus)

### Current users are not the target

Every product has a lifecycle. I'm sure you heard of it. First come early
adopters then early majority, etc.

[Technology adoption life cycle](https://en.wikipedia.org/wiki/Technology_adoption_life_cycle)

It's important for company to follow this scheme. Good example would be
OnePlus. At first they appealed to the tech crowd. Their phones were running
CyanogenMod, the community-driven, free and open source Android-based OS. Only
later they introduced their own OxygenOS.

[CyanogenMod](https://en.wikipedia.org/wiki/CyanogenMod?lang=en)

Currently users that still use Firefox are the ones that are a bit more
tech-savvy. Gone are the days when half of the population used it. These users
need to be treated differently. Firefox should be configured to allow more UI
modifications by default. Recently Mozilla also removed possibility to load
unsigned add-ons[^4].

These nerdy people[^5] have their own needs. If you fulfill them they will scream
at the general population saying "Why won't you use this browser? This one is
the best. Trust me I'm a nerd". Seems like a very fast way to grow.

### Lack of features

If you look at the current browser market, you'll notice a pattern. Every
underdog that's gaining popularity doesn't do that being just a good browser.

Brave has all the features a "cryptobro" will ever need. From personal wallet
to those shady ad-ralated schemes. Also: TOR mode.

Vivaldi is an all-in-one browser. It comes with ad-blocker, mail, calendar app,
RSS feed reader, translator, note app and many, many other features that make
it really stand out.

Opera has a gaming edition. Personally this gives me dreads but it is being
respected by it's own audience. It has extra features compared to standard
Opera. Things like limiting tab resources or usage indicator. Not to mention
the crazy aesthetics.

Even normal Opera had it's own go with build-in VPN. I've had few friends who
installed it just for this feature

What does the fox say? It has a privacy dashboard and a screenshoting tool.
Let's not forget about Pocket...... ok, let's be real. Who even uses that?

All of that is either useless or already implemented by the competition.
However, there is one exception: the engine. Sure it adds personality but is it
convincing. Only you, me and 4 other people around the globe know about this
(excluding Mozilla employees).

[^1]: While add-ons where supported since very early builds, a civilized way of
  installing them was only introduced circa 2021.

[^2]: Automated tests tool for web applications.

[^3]: The javascript engine used in Firefox.

[^4]: Previously this was possible with modifications in about:config page.

[^5]: This group of people includes me.
