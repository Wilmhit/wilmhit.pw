Ranked by average response times.

### Signal:
 - By my phone number if you happen to know it

### Matrix
 - @wilmhit:matrix.org

### Fediverse
 - @wilmhit@awawa.cat

### XMPP
 - wilmhit@pwned.life

### Email
 - wilmhit (at) disroot.org

### Codeberg
 - wilmhit

### Anilist
 - Wilmhit

### Gitlab
 - Wilmhit

### KDE Invent
 - wilmhit

### Pijul Nest
 - wilmhit

